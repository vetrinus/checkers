<?php


namespace vetrinus\checkers\figures;


use vetrinus\checkers\board\Cell;

class Unit
{
    /** @var Cell */
    protected $cell;

    /**
     * Unit constructor.
     * @param Cell $cell
     */
    public function __construct(Cell $cell)
    {
        $this->cell = $cell;
    }

    public function setCell(Cell $cell): void
    {
        $this->cell = $cell;
    }

    /**
     * @return Cell
     */
    public function getCell(): Cell
    {
        return $this->cell;
    }
}
