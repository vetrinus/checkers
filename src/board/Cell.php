<?php


namespace vetrinus\checkers\board;


class Cell
{
    /** @var int */
    private $x;
    /** @var int */
    private $y;
    /** @var bool */
    private $isDark;

    /**
     * Cell constructor.
     * @param int  $x
     * @param int  $y
     * @param bool $isDark
     */
    public function __construct(int $x, int $y, bool $isDark)
    {
        $this->x = $x;
        $this->y = $y;
        $this->isDark = $isDark;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function isDark(): bool
    {
        return $this->isDark;
    }

    public function equals(Cell $other): bool
    {
        return $this->getX() == $other->getX() && $this->getY() == $other->getY();
    }
}
