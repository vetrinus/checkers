<?php


namespace vetrinus\checkers\board;


use RuntimeException;
use vetrinus\checkers\figures\KingUnit;
use vetrinus\checkers\figures\Unit;

class UnitCollection
{
    /** @var Unit[] */
    private $units;

    /**
     * UnitCollection constructor.
     * @param Unit[] $units
     */
    public function __construct(array $units)
    {
        $this->units = $units;
    }

    public function get(Cell $coordinates): Unit
    {
        return $this->units[$this->getUnitIdx($coordinates)];
    }

    public function remove(Cell $coordinates): void
    {
        unset($this->units[$this->getUnitIdx($coordinates)]);
    }

    private function getUnitIdx(Cell $coordinates): int
    {
        foreach ($this->units as $unitIdx => $figure) {
            if ($figure->getCell()->equals($coordinates)) {
                return $unitIdx;
            }
        }

        throw new RuntimeException(sprintf(
            'Unable to find a unit on x = %d, y = %d',
            $coordinates->getX(),
            $coordinates->getY()
        ));
    }

    public function upgradeUnitToKing(Cell $coordinates): void
    {
        $idx = $this->getUnitIdx($coordinates);
        $unit = $this->units[$idx];

        if ($unit instanceof KingUnit) {
            throw new RuntimeException('Unit is already a king!');
        }

        $this->units[$idx] = new KingUnit($coordinates);
    }

    public function getFiguresCount(): int
    {
        return count($this->units);
    }
}
