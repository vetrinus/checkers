<?php


namespace vetrinus\checkers\board;


use vetrinus\checkers\figures\Unit;
use Webmozart\Assert\Assert;

class Board
{
    private const MIN_AXIS_LENGTH = 1;

    /** @var array */
    private $cells;
    /** @var UnitCollection */
    private $whiteCollection;
    /** @var UnitCollection */
    private $blackCollection;

    public function __construct(int $xAxisLength, int $yAxisLength)
    {
        Assert::greaterThanEq($yAxisLength, 8, 'Length of Y axis cannot be less than 8');
        Assert::true($yAxisLength % 2 == 0, 'Length of Y axis must even');
        Assert::greaterThanEq($xAxisLength, 8, 'Length of X axis cannot be less than 8');

        $this->initCells($xAxisLength, $yAxisLength);
        $this->initFigureCollections();
    }

    public function getHorizontalAxisLength(): int
    {
        return count($this->cells);
    }

    public function getVerticalAxisLength(): int
    {
        return count($this->cells[self::MIN_AXIS_LENGTH]);
    }

    public function getCell(int $x, int $y): Cell
    {
        Assert::range($x, self::MIN_AXIS_LENGTH, $this->getHorizontalAxisLength());
        Assert::range($y, self::MIN_AXIS_LENGTH, $this->getVerticalAxisLength());

        return $this->cells[$x][$y];
    }

    protected function initCells(int $xAxisLength, int $yAxisLength): void
    {
        $startCellIsWhite = false;

        for ($xAxis = 1; $xAxis <= $xAxisLength; $xAxis++) {
            $this->cells[$xAxis] = $this->createColumn($yAxisLength, $xAxis, $startCellIsWhite);
            $startCellIsWhite = !$startCellIsWhite;
        }
    }

    protected function createColumn(int $length, int $columnNumber, bool $startCellColor): array
    {
        $column = [];

        for ($i = 1; $i <= $length; $i++) {
            $column[$i] = new Cell($columnNumber, $i, $startCellColor);
            $startCellColor = !$startCellColor;
        }

        return $column;
    }

    protected function initFigureCollections(): void
    {

    }
}
