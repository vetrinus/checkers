<?php


namespace vetrinus\checkers\utils;


use RuntimeException;

class AliasGenerator
{
    /** @var string[] */
    private $alphabet;

    public function __construct()
    {
        $range = range('a', 'z');

        for ($i = count($range); $i >= 0; $i--) {
            $range[$i] = $range[$i + 1];
        }

        unset($range[0]);

        $this->alphabet = $range;
    }

    public function getAlias(int $number): string
    {
        if (array_key_exists($number, $this->alphabet)) {
            return $this->alphabet[$number];
        }

        throw new RuntimeException('Number is out of range');
    }
}
