<?php


namespace vetrinus\checkers\utils;


use InvalidArgumentException;

class Vector
{
    /** @var int */
    private $x;
    /** @var int */
    private $y;

    public function __construct(int $x, int $y)
    {
        if ($x == 0 && $y == 0) {
            throw new InvalidArgumentException('Null vector are not allowed');
        }

        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function getModule(): float
    {
        return sqrt(pow($this->x, 2) + pow($this->y, 2));
    }

    public function getAngleBetweenAxis(): int
    {
        return $this->getAngle(new Vector(1, 0));
    }

    public function getAngle(Vector $vector): int
    {
        $scalarMultiplication = $this->x * $vector->getX() + $this->y * $vector->getY();
        $modulesMultiplication = $this->getModule() * $vector->getModule();

        return rad2deg(acos($scalarMultiplication / $modulesMultiplication));
    }
}
