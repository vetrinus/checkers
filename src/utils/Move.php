<?php


namespace vetrinus\checkers\utils;


use vetrinus\checkers\board\Cell;

class Move
{
    /** @var Cell */
    private $start;
    /** @var Cell */
    private $end;
    /** @var Vector */
    private $vector;

    public function __construct(Cell $start, Cell $end)
    {
        $this->start = $start;
        $this->end = $end;

        $this->vector = new Vector(
            $end->getX() - $start->getX(),
            $end->getY() - $start->getY()
        );
    }

    public function getVector(): Vector
    {
        return $this->vector;
    }
}
