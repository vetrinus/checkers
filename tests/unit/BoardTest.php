<?php

use vetrinus\checkers\board\Board;
use vetrinus\checkers\board\Cell;

class BoardTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testCreateSuccess()
    {
        $x = 8;
        $y = 8;

        $board = new Board($x, $y);
        $this->tester->assertInstanceOf(Cell::class, $board->getCell($x, $y));
    }

    public function testUnexistedCell()
    {
        $this->tester->expectThrowable(new InvalidArgumentException('Expected a value between 1 and 8. Got: 9'),
            function () {
                $board = new Board(8, 8);
                $this->tester->assertInstanceOf(Cell::class, $board->getCell(9, 8));
            });
    }

    public function testCellColors()
    {
        $board = new Board(8, 8);

        $expect = [
            [
                false,
                true,
            ],
            [
                true,
                false,
            ],
        ];

        foreach ($expect as $xCoordinate => $row) {
            foreach ($row as $yCoordinate => $isDark) {
                $this->tester->assertTrue(
                    $isDark == $board->getCell($xCoordinate + 1, $yCoordinate + 1)->isDark()
                );
            }
        }
    }
}
