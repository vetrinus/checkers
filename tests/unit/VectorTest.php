<?php

use vetrinus\checkers\utils\Vector;

class VectorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _getAngleCases(): array
    {
        return [
            [new Vector(1, 1), 45],
            [new Vector(0, 1), 90],
            [new Vector(-1, 1), 135],
            [new Vector(-1, 0), 180],
            [new Vector(-1, -1), 225],
            [new Vector(0, -1), 270],
            [new Vector(1, -1), 315],
        ];
    }

    /**
     * @param Vector $vector
     * @param int    $angle
     * @dataProvider _getAngleCases
     */
    public function testAngleIsCorrect(Vector $vector, int $angle)
    {
        $this->tester->assertEquals($angle, $vector->getAngleBetweenAxis());
    }

    public function testCreateSuccess()
    {
        $vector = new Vector(
            $x = 10,
            $y = 15
        );

        $this->tester->assertEquals($x, $vector->getX());
        $this->tester->assertEquals($y, $vector->getY());
    }
}
